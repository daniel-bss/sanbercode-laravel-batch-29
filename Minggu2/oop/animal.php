<?php
// ==== RELEASE 0 ====
/*
Buatlah class Animal tersebut di dalam file animal.php.Lakukan instance terhadap class Animal tersebut di file index.php. Lakukan import class Animal dari animal.php di dalam index.php menggunakan require atau require once 
*/

// $sheep = new Animal("shaun");

// echo $sheep->name; // "shaun"
// echo $sheep->legs; // 4
// echo $sheep->cold_blooded; // "no"

// // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

class Animal {
    public function __construct($name) {
        // $this->name = $name . "<br>";
        $this->name = "Name: {$name} <br>";
        $this->legs = "legs: 4 <br>";
        $this->cold_blooded = "cold blooded: no <br>";
    }
}

?>
