<?php
// ==== RELEASE 1 ====
/*
Buatlah class Frog dan class Ape yang merupakan inheritance dari class Animal. Masing-masing class dibuat ke dalam satu file (Frog.php & Ape.php). Perhatikan bahwa Ape (Kera) merupakan hewan berkaki 2, hingga dia tidak menurunkan sifat jumlah kaki 4. class Ape memiliki function yell() yang mengeprint “Auooo” dan class Frog memiliki function jump() yang akan mengeprint “hop hop”.
*/

// // index.php
// $sungokong = new Ape("kera sakti");
// $sungokong->yell() // "Auooo"

// $kodok = new Frog("buduk");
// $kodok->jump() ; // "hop hop"

include_once 'animal.php';

class Ape extends Animal {
    function __construct($name) {
        parent:: __construct($name); // inherit Animal, tapi kita mau override this.legs nya...
        $this->legs = 2 . "<br>";
    }

    function yell() {
        echo "Yell: Auooo <br>";
    }
}

?>
