<?php
include 'animal.php';   
include 'Ape.php';
include 'Frog.php';

echo ">>SHEEP: <br>";

$sheep = new Animal("shaun");
echo $sheep->name; // "shaun"
echo $sheep->legs; // 4
echo $sheep->cold_blooded; // "no"

echo "<br> >>FROG <br>";

$kodok = new Frog("buduk");
echo $kodok->name; // "buduk"
echo $kodok->legs; // 4
echo $kodok->cold_blooded; // "no"
$kodok->jump(); // "hop hop"

echo "<br> >>APE <br>";

$sungokong = new Ape("kera sakti");
echo $sungokong->name; // "kera sakti"
echo $sungokong->legs; // 2
echo $sungokong->cold_blooded; // "no"
$sungokong->yell(); // "Auooo"

?>
