<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php 
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";
        /* 
            Soal No 1 
            Looping I Love PHP
            Lakukan Perulangan (boleh for/while/do while) sebanyak 20 iterasi. Looping terbagi menjadi dua: Looping yang pertama Ascending (meningkat) 
            dan Looping yang ke dua menurun (Descending). 

            Output: 
            LOOPING PERTAMA
            2 - I Love PHP
            4 - I Love PHP
            6 - I Love PHP
            8 - I Love PHP
            10 - I Love PHP
            12 - I Love PHP
            14 - I Love PHP
            16 - I Love PHP
            18 - I Love PHP
            20- I Love PHP
            LOOPING KEDUA
            20 - I Love PHP
            18 - I Love PHP
            16 - I Love PHP
            14 - I Love PHP
            12 - I Love PHP
            10 - I Love PHP
            8 - I Love PHP
            6 - I Love PHP
            4 - I Love PHP
            2 - I Love PHP
        */
        // Lakukan Looping Di Sini
        echo "<h4 style= 'margin: 0'>LOOPING PERTAMA</h4>";
        $i = 0;
        $love = " - I love PHP <br>";
        while ($i < 20) {
            $i += 2; // tambahkan 2, jika $i masih di bawah 20
            echo $i, $love;
        }
        echo "<h4 style= 'margin: 0'>LOOPING KEDUA</h4>";
        while ($i > 0) {
            echo $i, $love;
            $i -= 2; // kurangkan 2, jika $i masih di atas 0
        }

        echo "<h3>Soal No 2 Looping Array Modulo </h3>";
        /* 
            Soal No 2
            Looping Array Module
            Carilah sisa bagi dengan angka 5 dari setiap angka pada array berikut.
            Tampung ke dalam array baru bernama $rest 
        */

        $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: ";
        print_r($numbers);
        // Lakukan Looping di sini

        echo "<br>";
        echo "Array sisa baginya adalah:  "; 
        echo "<br>";
        $rest = []; // siapkan array kosong
        for ($i = 0; $i < count($numbers); $i++) { // iterasi sebanyak jumlah item array
            $rest[] = $numbers[$i] % 5; // push array $rest, yang berisi "sisa bagi" item setelah dibagi 5
        }
        print_r($rest);
        
        echo "<br><br>";

        $habis_dibagi_5 = [];
        echo "Array berisi angka yang habis dibagi 5 adalah: ";
        echo "<br>";    
        for ($i = 0; $i < count($numbers); $i++) {
            if ($numbers[$i] % 5 == 0) {
                $habis_dibagi_5[] = $numbers[$i]; // push array $habis_dibagi_5, jika ada item yang habis dibagi 5
            }
        }
        print_r($habis_dibagi_5);

        echo "<h3> Soal No 3 Looping Asociative Array </h3>";
        /* 
            Soal No 3
            Loop Associative Array
            Terdapat data items dalam bentuk array dimensi. Buatlah data tersebut ke dalam bentuk Array Asosiatif. 
            Setiap item memiliki key yaitu : id, name, price, description, source. 
            
            Output: 
            Array ( [id] => 001 [name] => Keyboard Logitek [price] => 60000 [description] => Keyboard yang mantap untuk kantoran [source] => logitek.jpeg ) 
            Array ( [id] => 002 [name] => Keyboard MSI [price] => 300000 [description] => Keyboard gaming MSI mekanik [source] => msi.jpeg ) 
            Array ( [id] => 003 [name] => Mouse Genius [price] => 50000 [description] => Mouse Genius biar lebih pinter [source] => genius.jpeg ) 
            Array ( [id] => 004 [name] => Mouse Jerry [price] => 30000 [description] => Mouse yang disukai kucing [source] => jerry.jpeg ) 

        */
        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];
        
        // Output: 
        $items_assoc = [];
        foreach ($items as $item) { // lakukan iterasi setiap item pada associative array menggunakan "foreach as"
            $items_assoc[] = [
                // tulis setiap nama key sesuai values-nya, dengan mengambil item sesuai index-nya 
                "id" => $item[0],
                "name" => $item[1],
                "price" => $item[2],
                "description" => $item[3],
                "source" => $item[4]
            ];
        }
        
        echo "OUTPUT: <br>";
        // Cara 1: menggunakan print_r()
        // foreach ($items_assoc as $item) {
        //     print_r($item); echo "<br>";
        // }

        // Cara 2: memisahkan antara keys dan values:
        foreach($items_assoc as $item) {
            echo "Array ( "; // setiap iterasi akan dimulai oleh string ini 
            foreach ($item as $key => $value) {
                // kemudian dilanjutkan secara berurutan oleh keys dan values-nya
                echo "[$key] => $value ";
            }
            // keluar dari loop $item, dan ditutup oleh:
            echo " ) <br>";
        }
        
        echo "<h3>Soal No 4 Asterix </h3>";
        /* 
            Soal No 4
            Asterix 5x5
            Tampilkan dengan looping dan echo agar menghasilkan kumpulan bintang dengan pola seperti berikut: 
            Output: 
            * 
            * * 
            * * * 
            * * * * 
            * * * * *
        */
        echo "Asterix: ";
        echo "<br>";
        $ukuran = 5; // ukurang pola bintang yang diinginkan
        for ($i = 0; $i < $ukuran; $i++) { // loop untuk sebanyak tinggi pola yang diinginkan
            $bintang = ''; // setiap iterasi, inisialisasi ulang sebuah string kosong
            for ($j = 0; $j < $i + 1; $j++) {
                $bintang .= '*'; // tambahkan bintang sebanyak jumlah iterasi index $i 
            }
            echo $bintang, "<br>";
        }
    ?>

</body>
</html>