// Ceritanya ini data dari database:
var items = [
    ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpg'], 
    ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpg'],
    ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
    ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpg']
]

// Tangkap tag <div class="row col-md-12 mt-2"  id="listBarang" >
const isi_toko = document.getElementById("listBarang");

function printItems(items) {
    var item_jualan = '';
    for (var i = 0; i < items.length; i++) {
        item_jualan += `
        <div class ="col-4 mt-2"> 
            <div class="card" style="width: 18rem;">
                <img src="./img/${items[i][4]}" class="card-img-top" alt="${items[i][1]}">
                <div class="card-body">
                    <h5 class="card-title" id="itemName">${items[i][1]}</h5>
                    <p class="card-text" id="itemDesc">${items[i][3]}</p>
                    <p class="card-text">Rp${items[i][2]/1000}.000,00</p>
                    <a class="btn btn-primary" id="addCart" onclick="tambahkan()">Tambahkan ke keranjang</a>
                </div>
            </div>
        </div>
        `;
    }
    isi_toko.innerHTML = item_jualan;
}

printItems(items);

function filter_(keyword, items) {
    let filtered = []
    for (let i = 0; i < items.length; i++) {
        if (items[i][1].toLowerCase().includes(keyword.toLowerCase())) {
            filtered.push(items[i])
        }
    }
    return filtered;
}

let formItem = document.getElementById("formItem");
formItem.addEventListener("submit", function(e) {
    e.preventDefault()
    let keyword = document.getElementById("keyword").value;
    printItems(filter_(keyword, items));
})

let addCart = document.getElementById("addCart");
let cart = document.getElementById("cart"); 
let jumlah = 0;

function tambahkan() {
    jumlah++;
    cart.innerHTML = `<i class="fas fa-shopping-cart"></i>(${jumlah})`; 
}